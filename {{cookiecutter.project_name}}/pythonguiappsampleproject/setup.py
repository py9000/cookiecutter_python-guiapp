import sys
from setuptools import setup, find_packages

from pythonguiappsampleproject.settings import *

assert sys.version_info >= MINIMUM_PYTHON_VERSION

setup(
    name="python-guiapp-sample-project",
    version=VERSION,
    description="python guiapp sample project",
    url="https://gitlab.com/terminallabs/sample-project/sample-project_python-guiapp",
    author="Terminal Labs",
    author_email="solutions@terminallabs.com",
    license="mit",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        "setuptools",
        "utilities-package@git+https://gitlab.com/terminallabs/utilitiespackage/utilities-package.git@master#egg=utilitiespackage&subdirectory=utilitiespackage",
    ],
    entry_points="""
      [console_scripts]
      pythonguiappsampleproject=pythonguiappsampleproject.__main__:main
  """,
)
